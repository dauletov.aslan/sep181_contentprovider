package kz.astana.contentproviderapp;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private final Uri CONTACT_URI = Uri.parse("content://kz.astana.contentproviderapp.App/contacts");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Cursor cursor = getContentResolver().query(CONTACT_URI, null, null, null, null);
        startManagingCursor(cursor);

        String[] from = {"name", "phone"};
        int[] to = {android.R.id.text1, android.R.id.text2};
        SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(
                MainActivity.this,
                android.R.layout.simple_list_item_2,
                cursor,
                from,
                to
        );
        ListView listView = findViewById(R.id.listView);
        listView.setAdapter(simpleCursorAdapter);

        Button insert = findViewById(R.id.insert);
        Button update = findViewById(R.id.update);
        Button delete = findViewById(R.id.delete);

        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("name", "Aslan");
                contentValues.put("phone", "+77071012244");
                Uri newUri = getContentResolver().insert(CONTACT_URI, contentValues);
                Log.d("Hello", "Uri: " + newUri);
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("name", "Max Pupkin");
                contentValues.put("phone", "+77777777777");
                Uri uri = ContentUris.withAppendedId(CONTACT_URI, 2);
                int updateCount = getContentResolver().update(uri, contentValues, null, null);
                Log.d("Hello", "Updated: " + updateCount);
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = ContentUris.withAppendedId(CONTACT_URI, 3);
                int deleteCount = getContentResolver().delete(uri, null, null);
                Log.d("Hello", "Deleted: " + deleteCount);
            }
        });
    }
}