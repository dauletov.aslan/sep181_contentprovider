package kz.astana.contentproviderapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class MyDatabaseHelper extends SQLiteOpenHelper {

    private final String[] contactNames = {"Vasya Pupkin", "John Smith"};
    private final String[] contactPhones = {"+77001112233", "+77004445566"};

    public MyDatabaseHelper(@Nullable Context context) {
        super(context, "contacts", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE contacts(" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "name TEXT, " +
                "phone TEXT" +
                ");");
        for (int i = 0; i < contactNames.length; i++) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("name", contactNames[i]);
            contentValues.put("phone", contactPhones[i]);
            db.insert("contacts", null, contentValues);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
